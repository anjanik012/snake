# Snake

Snake is the clone of old Nokia Snake game, written in C.

Note:Only Works on POSIX systems.

## Dependencies

ncurses

## Download

Install git from your package manager.

```bash
git clone https://gitlab.com/anjanik012/snake.git
cd snake
```

## Compile

```bash
gcc snake.c -o snake -lncurses -lpthread
```

## Play

```bash
./snake
```
## License

[WTFPL](http://www.wtfpl.net/about/)

## Original Author

Anjani Kumar

email: anjanik012@gmail.com
